/* Determine the largest of three integer quantities */
#include <stdio.h>

int maximum(int a, int b) {
    return (a > b) ? a : b;
}

int main() {
    int a, b, c, d;
    
    /* Read input data */
    printf("a = ");
    scanf("%d", &a);

    printf("b = ");
    scanf("%d", &b);

    printf("c = ");
    scanf("%d", &c);

    /* Peform comparison */
    d = maximum(a, b);
    d = maximum(c, d);

    printf("%d is the largest number.\n", d);
    
}
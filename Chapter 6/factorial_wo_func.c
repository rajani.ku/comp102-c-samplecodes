#include <stdio.h>


int main()
{
    int n, r, combination;

    printf("Value of n and r:");
    scanf("%d%d", &n, &r);

    int i, fact_n, fact_nr, fact_r;

    // Compute n!
    fact_n = 1;
    for (i = 1; i <= n; ++i)
    {
        fact_n = fact_n * i;
    }

    // Compute (n-r)!
    fact_nr = 1;
    for (i = 1; i <= (n-r); ++i)
    {
        fact_nr = fact_nr * i;
    }

    // Compute r!
    fact_r = 1;
    for (i = 1; i <= r; ++i)
    {
        fact_r = fact_r * i;
    }

    combination = fact_n / fact_nr * fact_r;
        
    printf("%dC%d = %d\n", n, r, combination);
}

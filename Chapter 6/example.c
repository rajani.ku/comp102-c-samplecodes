#include <stdio.h>
#include <stdlib.h>

int factorial(int n) {
    int f = 1;

    for (int i = 1; i <= n; i++) {
        f = f * i;
    }

    return f;
}

int sum(int a, int b) {
    return a + b;
}

int main() {
    int a;
    int num, fact = 1;

    printf("Enter an integer:");
    scanf("%d", &num);

    fact = factorial(num);

    printf("%d! = %d\n", num, factorial(num));

    return 0;
}

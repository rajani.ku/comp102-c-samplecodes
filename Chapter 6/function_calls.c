#include <stdio.h>

void modify(int a) {
    a = 100;
}

int main() {
    int x = 10;

    printf("Value of x before calling modify() = %d\n", x); 

    modify(x);
    printf("Value of x after calling modify() = %d\n", x);

    return 0;
}
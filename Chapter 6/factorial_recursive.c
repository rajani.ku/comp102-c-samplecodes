#include <stdio.h>

int factorial(int);

int main()
{
    int n, r, combination;

    printf("Value of n:");
    scanf("%d", &n);

    
    printf("%d! = %d\n", n, factorial(n));
}

int factorial(int n)
{
    printf("Calling factorial(%d)\n", n);
    
    int result;
    if (n <= 1) {
        result = 1;
    }
    else {
        result = (n * factorial(n - 1));
    }

    printf("factorial(%d) returns %d.\n", n, result);
    return result;
}

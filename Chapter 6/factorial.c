#include <stdio.h>

int factorial(int);

int main()
{
    int n, r, combination;

    printf("Value of n and r:");
    scanf("%d%d", &n, &r);

    combination = factorial(n) / (factorial(n - r) * factorial(r));
    printf("%dC%d = %d\n", n, r, combination);
}

int factorial(int n)
{
    int i;
    int fact = 1;

    for (i = 1; i <= n; ++i)
    {
        fact = fact * i;
    }
    return fact;
}
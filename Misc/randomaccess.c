#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct Student
{
    int rollno;
    char name[30];
};

int main()
{

    int rollno;
    printf("Enter rollno: ");
    scanf("%d", &rollno);

    FILE *fp = fopen("students.dat", "rb+");

    if (fp == NULL) 
    {
        printf("Could not read file!\n");
        exit(1);
    }

    long student_size = sizeof(struct Student);
    fseek(fp, ((rollno - 1) * student_size), SEEK_SET);

    struct Student s;
    fread(&s, student_size, 1, fp);

    // Print what was read
    printf("The name of the student whose roll number is %d is %s.\n", rollno, s.name);

    // Update name
    strcpy(s.name, "Davidson");

    fseek(fp, -student_size, SEEK_CUR);
    fwrite(&s, student_size, 1, fp);

    fclose(fp);
}

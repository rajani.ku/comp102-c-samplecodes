#include <stdio.h>

struct Student
{
    int rollno;
    char name[30];
};

int main()
{
    struct Student students[] = {
        {1, "Alice"},
        {2, "Alex"},
        {3, "David"},
        {4, "Bob"}};

    FILE *fout = fopen("students.dat", "wb");
    
    fwrite(students, sizeof(struct Student), 4, fout);
    
    fclose(fout);
}

#include <stdio.h>

struct Student
{
    int rollno;
    char name[30];
};

int main()
{
    struct Student students[4];

    FILE *fin = fopen("students.dat", "rb");

    fseek(fin, 0, SEEK_END);
    int n = ftell(fin) / sizeof(struct Student);

    rewind(fin);
    fread(students, sizeof(struct Student), n, fin);
    fclose(fin);

    // Print what was read
    for (int i = 0; i < n; i++)
    {
        printf("%d \t %s\n", students[i].rollno, students[i].name);
    }
}

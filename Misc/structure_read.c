#include <stdio.h>
#include <stdlib.h>

struct Student
{
    int rollno;
    char name[30];
};

int main()
{
    // Open the file for reading
    FILE *fin = fopen("students.csv", "r");

    if (fin == NULL)
    {
        printf("Could not read file.\n");
        exit(1);
    }

    // Count the number of lines (Number of lines = number of students + 1)
    char c;
    int count = 0;
    do
    {
        c = getc(fin);
        if (c == '\n')
        {
            count++;
        }

    } while (c != EOF);

    // Create an array to store students info
    struct Student *students = malloc((count - 1) * sizeof(struct Student));

    // Go to the beginning of the file
    rewind(fin);

    // Skip the first line
    do
    {
        c = getc(fin);
    } while (c != '\n');

    for (int i = 0; i < count - 1; i++)
    {
        fscanf(fin, "%d,%s\n", &students[i].rollno, students[i].name);
    }

    // Print what was read
    for (int i = 0; i < count - 1; i++)
    {
        printf("%d \t %s\n", students[i].rollno, students[i].name);
    }
    fclose(fin);
}

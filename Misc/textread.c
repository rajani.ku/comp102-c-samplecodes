#include <stdio.h>
#include <stdlib.h>

int main()
{
    FILE *fp = fopen("myfile.txt", "r"); // Open a file for reading

    if (fp == NULL)
    {
        printf("File could not be read.\n");
        exit(1);
    }
    
    float arr[5];

    // Write some floating-point numbers
    for (int i = 0; i < 5; i++)
    {
        fscanf(fp, "%f", &arr[i]);
    }

    // Print what was read from the file
    for (int i = 0; i < 5; i++)
    {
        printf("%8.2f", arr[i]);
    }

    char c = getc(fp);
    printf("%c", c);
    c = getc(fp);
    printf("%c", c);
    c = getc(fp);
    printf("%c", c);

    for (int i = 0; i < 40; i++)
    {
        // Read the character
        fscanf(fp, "%c", &c);
        // Print what was read
        printf("%c", c);
    }

    printf("\n\n");
    rewind(fp);
    do {
        c = getc(fp);
        printf("%c", c);
    } while (c != EOF);

    fclose(fp); // Close the file
}
#include <stdio.h>

int main() { 
    FILE *fp = fopen("mybinaryfile.dat", "wb"); // Open a binary file for writing

    float arr[] = {30.6, 40.3, 50, 60, 70};

    // Write some floating-point numbers
    // for (int i = 0; i < 5; i++) {
    //     fwrite(&arr[i], sizeof(arr[i]), 1, fp);
    // }

    fwrite(arr, sizeof(float), 5, fp);

   //fprintf(fp, "%s", "\n\n");

    // Write some characters
    char carr[40];
    for (int i = 0; i < 40; i++) 
    {
        carr[i] = '*';
    }
    fwrite(carr, sizeof(char), 40, fp);
    
    fclose(fp); // Close the file
}
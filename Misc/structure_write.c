#include <stdio.h>

struct Student
{
    int rollno;
    char name[30];
};

int main()
{
    struct Student students[] = {
        {1, "Alice"},
        {2, "Alex"},
        {3, "David"},
        {4, "Bob"}};

    FILE *fout = fopen("students.csv", "w");

    // Write header
    fprintf(fout, "Roll number, Name\n");
    // Write data
    for (int i = 0; i < 4; i++) {
        fprintf(fout, "%d,%s\n", students[i].rollno, students[i].name);
    }
    
    fclose(fout);
}

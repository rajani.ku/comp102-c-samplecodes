#include <stdio.h>

int main() { 
    FILE *fp = fopen("myfile.txt", "a"); // Open a file for writing

    float arr[] = {30.6, 40.3, 50, 60, 70};

    // Write some floating-point numbers
    for (int i = 0; i < 5; i++) {
        fprintf(fp, "%8.2f", arr[i]);
    }

    fprintf(fp, "\n\n");
    putc('\n', fp);

    // Write some characters
    for (int i = 0; i < 40; i++) 
    {
        fprintf(fp, "*");
    }
    
    fclose(fp); // Close the file
}
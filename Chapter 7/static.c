/** @file static.c
 *  @brief Example for static variables   
 *
 *  This demonstrates how to use static variables. 
 *  
 *  @author: Byron Gottfried
 */

#include <stdio.h>

/** @brief Returns the nth number in the Fibonacci sequence.
 * 
 *  This function makes use of a static variable to store the previous
 *  two numbers in the Fibonacci sequence. To find the nth number in the
 *  sequence, the static variables are added if n is greater than 2, 
 *  otherwise if n = 1, 0 is returned and if n = 2, 1 is returned. 
 * 
 *  @param n The position of the number to be found
 *  @return The nth number in the Fibonacci sequence
 */
long int fibonacci(int n);

/** @brief The main function
 * 
 *  This is the entry point of the program. This program prints
 *  the first n number of the Fibonacci sequence. 
 * 
 *  @return 0
 */
int main()
{
    register int a;
    int n;
    printf ("How many Fibonacci numbers? ");
    scanf("%d", &n);

    // Print n numbers of the Fibonacci sequence
    for (int i = 1; i <= n; i++) 
    {
        printf("%ld ", fibonacci(i));
    }
    printf("\n");
    
    return 0;
}

long int fibonacci(int n)
{
    static long int f1 = 0, f2 = 1;  // Static variables to store the previous two numbers of the Fibonacci sequence
    long int f;  // The nth number in the sequence

    // The first and the second number in the sequence are 0 and 1. 
    // The remaining numbers are the sum of previous two numbers in the sequence.
    f = (n < 3) ? (n-1) : f1 + f2;  

    // Update the static variables so that we get the previous two numbers of the sequence 
    // in the next function call.
    f1 = f2;
    f2 = f;

    return f;
}
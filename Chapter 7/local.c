#include <stdio.h>

void demoAuto();

void func()
{
    int i = 100;
}

int main()
{
    func();
    printf("n= %d\n", n); // Error: n is undefined here

    {
        int i = 1;
        printf("i = %d\n", i);
    }

    printf("i = %d\n", i); // Error: i is undefined here

    demoAuto();
}

void demoAuto()
{
    int i = 5;
    {
        {
            int i = 8;
            printf("i = %d\n", i);
        }
        printf("i = %d\n", i);
    }
    printf("i = %d\n", i);
}

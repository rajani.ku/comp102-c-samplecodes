#include <stdio.h>      // A compiler directive

// Starting point for the execution of the program
int main() 
{                               // Start a block
    printf("Hello World!\n");   // Print the message "Hello World!"
}                               // End a block
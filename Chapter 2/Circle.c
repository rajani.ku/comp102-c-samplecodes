/* Program to calculate the area of a circle */
#include <stdio.h>

int main()
{
    int radius;
    float pi, area;

    pi = 3.1416;    // Value of pi

    radius = 5;     // Let the radius be 5
    area = pi * radius * radius;

    printf("Area of a circle with radius %d is %f.\n", radius, area);
}
/* 
Program to calculate the area of a circle.
Author: John
*/
#include <stdio.h>

#define PI 3.1416   

int main()
{
    float radius, area;

    /* Get the value of radius from the user*/
    printf("Enter the radius: ");
    scanf("%f", &radius);

    // Calculate the area
    area = PI * radius * radius;

    /* Print the result */
    printf("Area of a circle with radius %f is %f.\n", radius, area);
}
/*
Sum.c: A program to add 10 and 20
*/
#include <stdio.h>      

int main() 
{                               
    int sum;                // Declare a variable sum

    // Add 10 and 20 and assign the result to sum
    sum = 10 + 20;          
    printf("%d\n", sum);    // Print the result
}    

/* Program to calculate the area of a circle */
#include <stdio.h>

#define PI 3.1416   

int main()
{
    int radius;
    float pi, area;

    radius = 5;
    area = PI * radius * radius;

    printf("Area of a circle with radius %d is %f.\n", radius, area);
}
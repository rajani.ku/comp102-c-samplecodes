#include <stdio.h>

int main()
{
    printf("size of integer = %lu\n", sizeof(char));
    unsigned char i;
    i = 8;
    printf("Logical NOT %c\n", !i);
    printf("Bitwise NOT %c\n", ~i);

    int n;
    n = 8;
    printf("%d\n", ~n);
}
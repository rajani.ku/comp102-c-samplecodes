#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE 300

int main()
{

    float marks[100], sum = 0;
    int n;

    printf("How many students?\n");
    scanf("%d", &n);

    int a[MAX_SIZE] = {1, [2] = n+1, [5] = n+2};

    for (int i = 0; i < MAX_SIZE; i++)
    {
        printf("%d ", a[i]);
    }
    printf("\n");

    if (n > 100)
    {
        printf("Not enough space.\n");
        exit(0);
    }
    

    for (int i = 0; i < n; i++)
    {
        printf("Enter the marks obtained by student %d: ", i + 1);
        scanf("%f", &marks[i]);
    }

    for (int i = 0; i < n; i++)
    {
        sum += marks[i];
    }
    //avg /= n;

    printf("Average = %f\n", sum/n);
}
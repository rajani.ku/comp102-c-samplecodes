#include <stdio.h>
#define ROWS 3
#define COLS 4

int main()
{
    int matrix1[ROWS][COLS] = { 
                                {1, 2, 3, 4}, 
                                {1, 2, 3, 4}, 
                                {1, 2, 3, 4}
                              };
    
    char matrix2[ROWS][COLS] = {};

    printf("Size of int = %lu\n", sizeof(int));
    printf("Size of matrix1 = %lu\n", sizeof(matrix1));
    printf("Size of matrix2 = %lu\n", sizeof(matrix2));

    printf("Matrix 1: \n");
    for (int i = 0; i < ROWS; i++)
    {
        for (int j = 0; j < COLS; j++)
        {
            printf("Element [%d][%d] is at %p\n", i, j, &matrix1[i][j]);
        }
    }
    printf("\n");

    printf("Matrix 2: \n");
    for (int i = 0; i < ROWS; i++)
    {
        for (int j = 0; j < COLS; j++)
        {
            printf("Element [%d][%d] is at %p\n", i, j, &matrix2[i][j]);
        }
    }
    printf("\n");
}

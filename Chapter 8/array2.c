#include <stdio.h>

int main() {
    auto int a[5] = {4, 5, 6, 6};

    for(int i = 0; i < 5; i++) {
        printf("%d\t", a[i]);
    }
    printf("%ld \n", sizeof(int));
}
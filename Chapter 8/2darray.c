#include <stdio.h>
#define ROWS 3
#define COLS 4

void display(const int [][COLS], int rows, int cols);

int main()
{
    int matrix1[ROWS][COLS] = { 
                                {1, 2, 3, 4}, 
                                {5, 6, 3, 4}, 
                                {1, 2, 3, 4}
                              };
    int matrix2[ROWS][COLS] = matrix1;
    int matrix3[ROWS][COLS] = {0};
    

    printf("Matrix 1:\n");
    display(matrix1, ROWS, COLS);

    printf("Matrix 2:\n");
    display(matrix2, ROWS, COLS);

    for (int i = 0; i < ROWS; i++)
    {
        for (int j = 0; j < COLS; j++)
        {
            
            matrix3[i][j] = matrix1[i][j] + matrix2[i][j];
        }
    }

    printf("Matrix 3:\n");
    display(matrix3, ROWS, COLS);

}

void display(const int m[][COLS], int rows, int cols) 
{
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            printf("%d ", m[i][j]);
        }
        printf("\n");
    }
}


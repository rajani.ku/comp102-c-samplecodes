#include <stdio.h>
#include <stdlib.h>
#define MAX 10

void multiply(int [], int, int);
void display(int[], int);

int main()
{

    int data[MAX];
    int n;

    printf("How many data?\n");
    scanf("%d", &n);

    if (n > MAX)
    {
        printf("Not enough space.\n");
        exit(0);
    }

    for (int i = 0; i < n; i++)
    {
        printf("Enter the data %d: ", i + 1);
        scanf("%d", &data[i]);
    }
    multiply(data, n, 2);
    display(data, n);
}

void multiply(int a[], int n, int m) {    
    for (int i = 0; i < n; i++)
    {
        a[i] = a[i] * m;
    }
}

void display(int a[], int n) {    
    for (int i = 0; i < n; i++)
    {
        printf("%d ", a[i]);
    }
    printf("\n");
}
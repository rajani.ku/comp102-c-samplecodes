#include <stdio.h>

int main() 
{
    int *ap[5] ;
    int a[4][4];
    int (*pa)[4] = a;

    printf("ap\n");
    for (int i = 0; i < 5; i++) {
        printf("%p\n", *(ap + i));
    }

    printf("pa\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            printf("%d\n", *(*(pa + i) + j));
        }
    }

    char *weekday[] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

    printf("Weekdays\n");
    for (int i = 0; i < 5; i++) {
        printf("%s\n", weekday[i]);
    }
}
#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE 300

int main()
{

    float *marks, sum = 0;
    int n;

    printf("How many students?\n");
    scanf("%d", &n);

    marks = (float *) malloc(n * sizeof(float));

    if (n > 100)
    {
        printf("Not enough space.\n");
        exit(0);
    }

    for (int i = 0; i < n; i++)
    {
        printf("Enter the marks obtained by student %d: ", i + 1);
        scanf("%f", &marks[i]);
    }

    for (int i = 0; i < n; i++)
    {
        sum += marks[i];
    }

    printf("Average = %f\n", sum/n);

    free(marks);
}
#include <stdio.h>

int main()
{
    int x = 5;
    int *p;
    p = &x;
    printf("x = %d, &x = %p\n", x, &x);
    printf("Size of integer = %lu\n", sizeof(x));

    printf("p+1 = %p\n", (p + 1));
    printf("p+5 = %p\n\n", (p + 5));

    char *pc, c;
    pc = &c;
    printf("pc = %p\n", pc);
    printf("Size of char = %lu\n", sizeof(c));

    printf("pc+1 = %p\n", (pc + 1));
    printf("pc+5 = %p\n", (pc + 5));
}
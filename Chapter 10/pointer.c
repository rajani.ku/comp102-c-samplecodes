#include <stdio.h>

struct Student
{
    int rollno;
    char name[30];
    float marks[3];
    float average;
};

void print(struct Student *s);

int main()
{
    int n = 3, sum = 0;
    struct Student student =
        {1, "Anne", {89, 88, 87}};

    struct Student *studentPtr = &student;

    for (int j = 0; j < 3; j++)
    {
        sum += studentPtr->marks[j];
    }
    student.average = sum / 3.0;

    printf("%s got %0.2f.\n", student.name, student.average);

    printf("\nPrinting using our print function.\n");
    print(studentPtr);
}

void print(struct Student *s)
{
    printf("%s got %0.2f.\n", s->name, s->average);
    printf("%s got %0.2f.\n", (*s).name, (*s).average);
}
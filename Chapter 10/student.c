#include <stdio.h>

struct Date
{
    int day, month, year;
};

struct Student
{
    int rollno;
    char name[30];
    float marks[3];
    struct Date dob;

    float average;
};

void print(struct Student s);
void printPtr(const struct Student *s);

int main()
{
    int n = 3, sum = 0;
    // struct Student students[] = {
    //     {1, "Anne", {89, 88, 87}, {.day=23, .month=3, .year=2000} },
    //     {2, "John", {76, 67, 89}},
    //     {3, "Alex", {69, 89, 91}}};

    struct Student students[n];

    for (int i = 0; i < n; i++)
    {
        printf("Enter details of student %d:\n", (i + 1));
        printf("Name: ");
        scanf("%s", students[i].name);
        printf("Roll number: ");
        scanf("%d%*c", &students[i].rollno);
        printf("Date of Birth (yyyy-mm-dd):");
        scanf("%d-%d-%d", &students[i].dob.year, &students[i].dob.month, &students[i].dob.day);

        printf("Enter marks obtained in \n");
        printf("Science: ");
        scanf("%f", &students[i].marks[0]);
        printf("Maths: ");
        scanf("%f", &students[i].marks[1]);
        printf("Computer programming: ");
        scanf("%f", &students[i].marks[2]);
    }

    for (int i = 0; i < n; i++) // for each student
    {
        sum = 0;
        // Add all marks obtained by student i
        for (int j = 0; j < 3; j++)
        {
            sum += students[i].marks[j];
        }
        students[i].average = sum / 3.0;
    }

    for (int i = 0; i < n; i++)
    {
        printf("%s got %0.2f.\n", students[i].name, students[i].average);
    }

    printf("\nPrinting using our printPtr function.\n");
    for (int i = 0; i < n; i++)
    {
        struct Student st = students[i];
        printPtr(&st);
    }

    printf("\nPrinting using our print function.\n");
    for (int i = 0; i < n; i++)
    {
        struct Student st = students[i];
        print(st);
    }
}

void print(struct Student s)
{
    printf("%s was born on %d-%d-%d.\n", s.name, s.dob.year, s.dob.month, s.dob.day);
    printf("%s got %0.2f.\n", s.name, s.average);
}

void printPtr(const struct Student *s)
{
    printf("%s got %0.2f.\n", s->name, s->average);
}
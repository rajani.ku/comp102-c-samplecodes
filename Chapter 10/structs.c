#include <stdio.h>

struct Student
{
    int rollno;
    char name[30];
    int percentage;
};

struct Time
{
    int hour;
    int minute;
    int second;
};

struct
{
    int day;
    int month;
    int year;
} date1, date2;

int main()
{
    struct Student s1 = {.name = "Ram", .rollno = 1, .percentage = 87.5};
    struct Time t1 = {10, 20, 30};

    printf("Size of Student = %lu\n", sizeof(s1)); // Padding / merory alignment
    printf("Size of Time = %lu\n", sizeof t1);
    printf("Size of date1 = %lu\n", sizeof date1);
}
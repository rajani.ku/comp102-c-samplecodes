#include <stdio.h>
#define PI 3.1416

int main()
{
    float radius, result;
    char choice;

    printf("Enter the radius: \n");
    scanf("%f%*c", &radius);    // %*c will read the newline from the buffer and discard it

    printf("Type 'C' to calculate circumference, or type 'A' to calculate area\n");
    scanf("%c", &choice);

    switch(choice) {
        case 'C': 
        case 'c':
            printf("Circumference is %f.\n", 2 * PI * radius);
            break;
        case 'A':
        case 'a':
            printf("Area is %f.\n", PI * radius * radius);
            break;
        default:
            printf("Invalid choice.\n");
    }
}
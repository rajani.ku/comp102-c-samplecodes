#include <stdio.h>

int main()
{
    int num;
    printf("Enter integers between 0 and 100:\n");
    do {
        scanf("%d", &num);
        if (num < 0) {
            printf("Please enter integers between 0 and 100\n");
            continue;
        }
        // .. process num
        // ...
        printf("You entered %d.\n", num);
    } while (num <= 100);
}





#include <stdio.h>

int main()
{
    int first, second;
    printf("Enter two integers: ");
    scanf("%d %d", &first, &second);

    if (first > second) {
        printf("First number is bigger\n"); // This line will be executed only if first is greater than second
    }
}
#include <stdio.h>

int main()
{
    int num;
    printf("Enter integers between 0 and 100:\n");
    do {
        scanf("%d", &num);
        if (num < 0) {
            printf("You entered a negative integer\n");
            break;
        }
        // .. process num
        // ...
        printf("You entered %d.\n", num);
    } while (num <= 100);
}
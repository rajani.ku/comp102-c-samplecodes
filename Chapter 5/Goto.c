#include <stdio.h>

int main() {
    int num;
    printf("Enter integers between 0 and 100:\n");

    read: 
    scanf("%d", &num);
    
    // .. process num
    // ...
    
    if (num > 0 && num <= 100)
        goto read;
}
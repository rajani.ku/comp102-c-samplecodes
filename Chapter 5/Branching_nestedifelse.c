#include <stdio.h>
#include <stdlib.h>

int main() {
    int a, b, c;

    /* Read input data */
    printf("a = ");
    scanf("%d", &a);

    printf("b = ");
    scanf("%d", &b);

    printf("c = ");
    scanf("%d", &c);

    /* Peform comparison */
    // if (a > b) {
    //     if (a > c) {
    //         printf("%d is the largest number.\n", a);
    //     } else {
    //         printf("%d is the largest number.\n", c);       
    //     }
    // } else if (b > c) {
    //     printf("%d is the largest number.\n", b);
    // } else {
    //     printf("%d is the largest number.\n", c);
    // }
    

    // if ((a > b) && (a > c)) {
    //     printf("%d is the largest number.\n", a);
    // } else if ((b > a) && (b > c)) {
    //     printf("%d is the largest number.\n", b);
    // } else {
    //     printf("%d is the largest number.\n", c);
    // }
    

    int largest;
    if ((a > b) && (a > c)) {
        largest = a;
    } else if ((b > a) && (b > c)) {
        largest = b;
    } else {
        largest = c;
    }
    printf("%d is the largest number.\n", largest);
}